RealVNC 6.11.0 is the last version that works with this license key.
Download URLS:

- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-x64.deb
- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-x86.deb
- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-ARM64.deb
- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-ARM.deb
- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-x64.rpm
- https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-x86.rpm


For Ubuntu: 
- `wget https://downloads.realvnc.com/download/file/vnc.files/VNC-Server-6.11.0-Linux-x64.deb`
- `apt install xfce4 ./VNC-Server-6.11.0-Linux-x64.deb xserver-xorg-video-dummy`
- `vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA`
- `vncinitconfig -enable-system-xorg`
- `nano /etc/vnc/config.d/common.custom`
- Add the following:
```
ConnectToExisting=1
SendPrimary=0
```
- `systemctl enable --now vncserver-virtuald`
- Connect on port 5999 and you can change resolution in display settings.
